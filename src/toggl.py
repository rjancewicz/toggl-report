
import math
import requests
import datetime
from enum import Enum, IntEnum
from urllib.parse import urljoin
from requests.auth import HTTPBasicAuth
from time import sleep
import time
import pytz
from termcolor import colored, cprint 

TOGGL_URL = "https://www.toggl.com/api/v8/"




class TimePreset(Enum):
  TODAY = 1
  YESTERDAY = 2
  THIS_WEEK = 3
  LAST_WEEK = 4
  THIS_MONTH = 5
  LAST_MONTH = 6
  THIS_YEAR = 7
  LAST_YEAR = 8

class Toggl(object):

  def __init__(self, token, base_url=TOGGL_URL):
    self.base_url = base_url
    self.token = token

    self.auth = HTTPBasicAuth(token, 'api_token')

    self.headers = {
      "Content-Type": "application/json"
    }

  @property
  def workspaces(self):
    res = self._get(Workspace.endpoint)
    return [Workspace(self, data=x) for x in res]

  def workspace(self, _id):
    return Workspace(self, _id)

  def client(self, _id):
    return Client(self, _id)

  def project(self, _id):
    return Project(self, _id)

  def date(self, date):
    return TogglDate(self, date)

  def timerange(self, start_date, end_date=None):
    return TogglTimeRange(self, start_date, end_date)




  def _request(endpoint, data, method="GET"):
    pass

  def _get(self, endpoint, params=None):

    url = urljoin(self.base_url, endpoint)
    # print(url)
    res = requests.get(url, params=params, auth=self.auth, headers=self.headers)
    # print(res.text)
    try:
      return res.json()
    except:
      print(res.text)
      print(res.headers)


class APIEndpoint(object):

  REMAP = {
    'id': "_id"
  }

  def __init__(self, api, _id=None, data=None):
    self.api = api

    if _id is not None:
      self._fetch(_id)

    if data is not None:
      self._apply_attrs(data)

  def _apply_attrs(self, data):

    for key, value in data.items():
      if key in self.REMAP:
        key = self.REMAP[key]
      setattr(self, key, value)

  def _fetch(self, _id):
    endpoint = f"{self.endpoint}/{_id}"
    item = self.api._get(endpoint)

    item = item.get('data', {})

    self._apply_attrs(item)

  def _list_owned(self, cls):

    endpoint = f"{self.endpoint}/{self._id}/{cls.endpoint}"

    res = self.api._get(endpoint)

    return [cls(self.api, data=x) for x in res]



class Workspace(APIEndpoint):
  endpoint = "workspaces"

  @property
  def projects(self):
    return self._list_owned(Project, )

  @property
  def clients(self):
    return self._list_owned(Client)

  @property
  def tags(self):
    return self._list_owned(Tags)

class Project(APIEndpoint):
  endpoint = "projects"

  @property
  def client(self):
    if hasattr(self, "cid"):
      return Client(self.api, _id=self.cid)
  

class Client(APIEndpoint):
  endpoint = "clients"

class Tags(APIEndpoint):
  endpoint = "tags"

  def __repr__(self):
    return f"Tag: {self.name}"



class TogglTimeRange(APIEndpoint):

  def __init__(self, api, start_date, end_date=None):
    self.api = api

    if isinstace(start_date, TimePreset):
      pass
    elif isinstace(start_date, datetime.datetime):
      pass
    else:
      pass
      # todo 

  @property
  def entries(self):
  
    endpoint = "time_entries"

    start_date = self.start_date.isoformat()
    end_date = self.end_date.isoformat()

    args = {
      'start_date': start_date,
      'end_date': end_date
    }

    res = self.api._get(endpoint, args)


    entries = [TimeEntry(self.api, data=entry) for entry in res]

    return entries



class TogglDate(TogglTimeRange):
  
  def __init__(self, api, date):
    self.api = api

    self.start_date = datetime.datetime.combine(date, datetime.datetime.min.time(), datetime.timezone.utc)
    self.start_date += datetime.timedelta(seconds=time.timezone)
    self.end_date = self.start_date + datetime.timedelta(days=1)

class TimeEntry(APIEndpoint):
  endpoint = "time_entries"

  @property
  def workspace(self):
    return Workspace(self.api, self.wid)

  @property
  def project(self):
    return Project(self.api, self.wid)

  @property
  def user(self):
    raise NotImplemented
    return None

if __name__ == "__main__":
  pass
