import os
from argparse import ArgumentParser
from configparser import ConfigParser
from datetime import date, datetime, timedelta
import sys
from termcolor import colored, cprint 
from time import sleep
from toggl import *
from os import environ
from pprint import pprint
from enum import IntEnum

class Weekdays(IntEnum):
  Monday    = 1
  Tuesday   = 2
  Wednesday = 3
  Thursday  = 4
  Friday    = 5
  Saturday  = 6
  Sunday    = 7

DEFAULT_CONFIG = os.path.join(os.path.dirname(os.path.abspath(__file__)), "config.ini")

def configure():

  parser = ArgumentParser(description="Toggle CLI Report")

  parser.add_argument('-c', '--config',
    default=DEFAULT_CONFIG,
    help="Configuration file")
  parser.add_argument('-t', '--token',
    help="Your Toggle API token (use ENV[TOGGL_TOKEN] or config file)")

  parser.add_argument('--week', nargs="+")

  parser.add_argument('--today', action='store_true', default=False)
  parser.add_argument('--yesterday', action='store_true', default=False)

  parser.add_argument('--sunday', dest='weekday', action='store_const',
                    const=Weekdays.Sunday)
  parser.add_argument('--monday', dest='weekday', action='store_const',
                    const=Weekdays.Monday)
  parser.add_argument('--tuesday', dest='weekday', action='store_const',
                    const=Weekdays.Tuesday)
  parser.add_argument('--wednesday', dest='weekday', action='store_const',
                    const=Weekdays.Wednesday)
  parser.add_argument('--thursday', dest='weekday', action='store_const',
                    const=Weekdays.Thursday)
  parser.add_argument('--friday', dest='weekday', action='store_const',
                    const=Weekdays.Friday)
  parser.add_argument('--saturday', dest='weekday', action='store_const',
                    const=Weekdays.Saturday)

  parser.add_argument('--client', help="")

  args = parser.parse_args()

  config = ConfigParser()

  config.read(args.config)

  return (config, args)

def _this_week():

  today = date.today()

  offset = (today.weekday() + 1) % 7

  return today - timedelta(days=offset)

def _last_week():

  return _this_week() - timedelta(days=7)

def report_range(args):

  date = datetime.date.today()
  days = 1

  if args.yesterday:
    date -= timedelta(days=1)

    if args.today:
      days += 1

  if args.week:

    days = 0

    if 'last' in args.week:
      date = _last_week()
      days += 7
    if 'this' in args.week:
      date = min(date, _this_week())
      days += 7


  if args.weekday:

    today = date.today()
    current = today.isoweekday()

    target = int(args.weekday)

    delta = 0

    if target >= current: # (last week)
      delta = 9 - target
    else: # current > args.weekday (this week)
      delta = current - target

    date = today - timedelta(days=delta)

  return date, days

def resolve_token(config, args):

  if 'TOGGL_TOKEN' in environ:
    return environ['TOGGL_TOKEN']

  if 'TOGGL' in config:
    if 'TOKEN' in config['TOGGL']:
      return config['TOGGL']['TOKEN']

  if args.token:
    return args.token

  return None

def main():

  (config, args) = configure()

  token = resolve_token(config, args)

  if token is None:
    print("No Toggl token. Exiting.")
    sys.exit(1)

  toggl = Toggl(token)

  date, days = report_range(args)

  company = None

  if 'TOGGL' in config:
    if 'COMPANY' in config['TOGGL']:
      company = config['TOGGL']['COMPANY'].upper()
  
  for _ in range(days):

    cprint(f"{date.strftime('%A [%m/%d]')}:", "magenta", attrs=['bold'])
    
    entries = toggl.date(date).entries

    pids = set([e.pid for e in entries])

    projects = [toggl.project(pid) for pid in pids]

    cids = set([p.cid for p in projects])

    clients = [toggl.client(cid) for cid in cids]

    if len(pids) == 0:
      cprint("No Hours", "red")
      continue

    day_total = 0
    real_total = 0

    billable = 0

    for client in clients:
      print()
      cprint(f"{client.name}:", "green")

      first = True
      for project in projects:
        if project.cid != client._id:
          continue

        if not first:
          print()
        else:
          first = False

        cprint(f"{project.name}", "blue")

        data = {}
        for entry in entries:
          if project._id != entry.pid:
            continue
          
          # skip currently running
          if entry.duration <= 0:
            continue

          if entry.description in data:
            data[entry.description] += entry.duration
          else:
            data[entry.description] = entry.duration

        time_total = 0
        for task, seconds in data.items():
          """
            300 seconds = 5 minutes
              round to the nearest five minutes
          """

          sec = 300 * math.floor((seconds + 300) / 300)
          print(f"{task} :: {sec/60/60:0.2f}")
          time_total += sec
          real_total += seconds

          if client.name.upper() != company:
            billable += sec

        time_total = 900 * math.floor((time_total + 899) / 900)
        hours = time_total/60/60
        cprint(f"-" * 40, "yellow")
        cprint(f"Project Total: {hours:0.2f}h", "yellow")

        day_total += hours

        sleep(2) # hacky way to do rate limiting add to the toggl class

    real_hours = real_total/60/60
    billable_hours = billable/60/60

    billable_pct = billable_hours / day_total * 100

    print()
    cprint(f"-" * 40, "yellow")
    cprint(f"Actual Hours : {real_hours:0.2f}h", "yellow")
    cprint(f"Logged Hours : {day_total:0.2f}h", "yellow")
    cprint(f"Billable     : {billable_hours:0.2f}h ({billable_pct:0.2f}%)", "yellow")


    date += datetime.timedelta(days=1)


if __name__ == "__main__":
  main()
